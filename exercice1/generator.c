#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <time.h>

#include "generator.h"

void simple_generator(const size_t file_length, const size_t alphabet_length) {
  srand(time(NULL));

  for (size_t i = 0; i < file_length; ++i) {
    printf("%c", generate_letter(alphabet_length));
  }
}

// OUTILS
char generate_letter(size_t length) {
  int number = (int) rand() % length;
  
  return (char) 48 + number;
}
