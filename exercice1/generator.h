#ifndef GENERATOR_H
#define GENERATOR_H

char generate_letter(size_t length);
void simple_generator(const size_t file_length, const size_t alphabet_length);

#endif //GENERATOR_H
