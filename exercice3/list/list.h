#include <stdio.h>
#include <stdlib.h>

struct _list {
   int startNode,        /* etat de depart de la transition */
       targetNode;       /* cible de la transition          */
   unsigned char letter; /* etiquette de la transition      */
   struct _list *next;   /* maillon suivant                 */
};

typedef struct _list *List;

//  list_insert : insère start, end, letter en queue de la liste associée à list. 
//    Renvoie la liste nouvellement formée.
void *list_insert(struct _list *list, int start, int end, unsigned char letter);

//  list_insert : renvoie une liste composée uniquement par la première cellule de la liste.
struct _list *list_get_first_elem(struct _list *list);

//  list_remove : retire la cellule en tête de la liste associée à list. 
//    Renvoie la nouvelle liste.
//    Si list vaut NULL, retourne une erreur.
struct _list *list_remove(struct _list *list);

//  is_in_list : la transition startNode, letter dans la table de hachage transition
//    existe-t-elle ?
//    Retourne targetNode si oui, -1 sinon.
int is_in_list(struct _list *t, unsigned char letter, int startNode);

//  list_print : affiche la liste list
void list_print(struct _list *list);
