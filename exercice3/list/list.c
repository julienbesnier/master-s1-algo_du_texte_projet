#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include "list.h"

#define EXIST_NO_TRANSITION -1

void *list_insert(struct _list *list, int start, int end, unsigned char letter) {
  struct _list *p = malloc(sizeof(struct _list));
  if (p == NULL) {
    perror("Problème lors de la création de la cellule.");
    exit (EXIT_FAILURE);
  }
  
  p -> startNode = start;
  p -> targetNode = end;
  p -> letter = letter;
  p -> next = NULL;

  if (list == NULL) {
    list = p;
    return list;
  } 
  
  struct _list *temp = list;
  while (temp -> next != NULL) {
    temp = temp -> next;
  }
  temp -> next = p;

  return list;
}

struct _list *list_get_first_elem(struct _list *list) {
  if (list == NULL) {
    perror("Suppression : la liste est vide");
    exit (EXIT_FAILURE);
  }

  struct _list *p = malloc(sizeof(struct _list));
  if (p == NULL) {
    perror("Problème lors de la création de la cellule.");
    exit (EXIT_FAILURE);
  }
  
  p -> startNode = list -> startNode;
  p -> targetNode = list -> targetNode;
  p -> letter = list -> letter;
  p -> next = NULL;

  return p;
}

struct _list *list_remove(struct _list *list) {
  if (list == NULL) {
    perror("Suppression : la liste est vide");
    exit (EXIT_FAILURE);
  }

  return list -> next;
}

int is_in_list(struct _list *t, unsigned char letter, int startNode) {
  while (t != NULL) {
    // recherche 
    if ((t -> letter == letter) && (t -> startNode == startNode)) {
      return t -> targetNode;
    }
    t = t -> next;
  }

  return EXIST_NO_TRANSITION;
}

void list_print(struct _list *list) {
  if (list == NULL) {
    printf("List -> X\n");
    return;
  }

  printf("List -> ");
  struct _list *current = list;
  while (current != NULL) {
    printf("[ %d | %d | %c ] -> ", 
        current -> startNode, 
        current -> targetNode, 
        current -> letter);
    current = current -> next;
  }

  printf("X\n");
}
