#include <stdio.h>
#include <stdlib.h>

struct _trie {
  int maxNode;      /* Nombre maximal de noeuds du trie    */
  int nextNode;     /* Indice du prochain noeud disponible */
  int **transition; /* listes d’adjacence                  */
  int *finite;     /* etats terminaux                      */
  int *suppleant;  /* etats suppleants                    */
};

typedef struct _trie *Trie;

// créer un trie (cette primitive prend en entrée le nombre maximal
// de nœuds du trie) ;  
Trie createTrie(int maxNode);

// insérer un mot dans un trie
void insertInTrie(Trie trie, unsigned char *w);

// teste si un mot est contenu dans un trie.
// retourne 1 si oui, 0 sinon.
int isInTrie(Trie trie, unsigned char *w);

// nettoye la structure Trie après utilisation
void matrice_dispose(Trie trie);

// OUTILS

// Affiche les trois tables de transition, de finitude et de suppléance.
void printTabs(Trie trie);
// Affiche les trois tables de transition (réduite), de finitude et de suppléance.
void printTabsReduced(Trie trie);
// Affiche la table de transition dans le terminal.
void printTransition(Trie trie);
// Affiche la table de transition réduite dans le terminal, 
// c'est à dire seulement pour les caractères de [a-z].
void printTransitionReduced(Trie trie);
// Affiche la table de finitude dans le terminal.
void printFinite(Trie trie);
// Affiche la table de suppleant dans le terminal.
void printSuppleant(Trie trie);
