#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>

#include "matrice.c"
#include "../fifo/fifo.c"
#include "../list/list.c"

#define EXIST_NO_TRANSITION -1

#define NOT_FINAL 0
#define FINAL 1

void aide(const char *progname);
//  ajouter_les_mots : fonction qui traite le fichier et insère les mots 
//  dans un trie.
void pre_AC(Trie t, char *file);
int ajouter_les_mots(const char *filename, Trie t);
void insertion_boucle_etat_zero(Trie t);
void completer(Trie t);

#define STRINGLEN_MAX 1000
#define FUN_SUCCESS 1
#define FUN_FAILURE -1

#define NODE_NUMBER 10000


//--- fonction principale

#define ARG__OPT_LONG      "--"
#define ARG_HELP           "help"
#define ARG_TIME           "time"
#define ARG_VERBOSE        "verbose"
#define ARG_VERBOSE_FULL   "verbosefull"

int main(int argc, char *argv[]) {
  int do_show_time = 0;
  int do_show_verbose = 0;
  int do_show_verbose_full = 0;
  
  for (int k = 1; k < argc; ++k) {
    const char *a = argv[k];
    if (strlen(a) > strlen(ARG__OPT_LONG)
        && strlen(a) <= strlen(ARG__OPT_LONG ARG_HELP)
        && strncmp(a, ARG__OPT_LONG ARG_HELP, strlen(a)) == 0) {
      aide(argv[0]);
      return EXIT_SUCCESS;
    }

    if (strlen(a) > strlen(ARG__OPT_LONG)
        && strlen(a) <= strlen(ARG__OPT_LONG ARG_TIME)
        && strncmp(a, ARG__OPT_LONG ARG_TIME, strlen(a)) == 0) {
      do_show_time = 1;
    }

    if (strlen(a) > strlen(ARG__OPT_LONG)
        && strlen(a) <= strlen(ARG__OPT_LONG ARG_VERBOSE)
        && strncmp(a, ARG__OPT_LONG ARG_VERBOSE, strlen(a)) == 0) {
      do_show_verbose = 1;
    }

    if (strlen(a) > strlen(ARG__OPT_LONG)
        && strlen(a) <= strlen(ARG__OPT_LONG ARG_VERBOSE_FULL)
        && strncmp(a, ARG__OPT_LONG ARG_VERBOSE_FULL, strlen(a)) == 0) {
      do_show_verbose_full = 1;
    }
  }
  
  if (argc != 3) {
    if (argc != 4 && do_show_time == 0 && do_show_verbose == 0) {
      printf("Erreur : veuillez entrer deux valeurs.\n");
      printf("Pour plus d'informations, veuillez utiliser la commande --help\n");

      exit(EXIT_FAILURE);
    }
  }

  // création et initilisation du Trie
  Trie trie = createTrie(NODE_NUMBER);
  pre_AC(trie, argv[1]);

  FILE *f = fopen(argv[2], "r");
  if (f == NULL) {
    return FUN_FAILURE;
  }

  int nb_occ = 0;

  int e = 0;
  int c = 0;
  while ((c = fgetc(f)) != EOF) {    
    while (trie -> transition[e][c] == EXIST_NO_TRANSITION) {
      e = trie -> suppleant[e];
    }
    
    e = trie -> transition[e][c];

    if (trie -> finite[e] != NOT_FINAL) {
      nb_occ = nb_occ + trie -> finite[e];
    }
  }

  if (fclose(f) != 0) {
    return EXIT_FAILURE;
  }

  // Affichage du résultat si l'option verbose est activée
  if (do_show_verbose == 1) {
    printTabsReduced(trie);
    printf("Nombre d'occurences : ");
  } else if (do_show_verbose_full == 1) {
    printTabs(trie);
    printf("Nombre d'occurences : ");
  }

  printf("%d\n", nb_occ);

  // Désallocation de la matrice qu'on n'utilisera plus
  matrice_dispose(trie);

  if (do_show_time == 1) {
    printf("\nLe programme a mis %f secondes à s'exécuter.\n\n",
      (double) clock()/CLOCKS_PER_SEC);
  }

  return EXIT_SUCCESS;
}

//-- initialisation de A-C
void pre_AC(Trie t, char *file) {
  // on y insère les mots
  int r = ajouter_les_mots(file, t);
  if (r != 1) {
    printf("Erreur lors de l'exécution du programme\n");
  }

  // on insère la boucle de l'état zéro
  insertion_boucle_etat_zero(t);

  // on le complète
  completer(t);
}


//--- Ajout des mots
int ajouter_les_mots(const char *filename, Trie t) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    return FUN_FAILURE;
  }
  int r = FUN_SUCCESS;

  int c = 0;
  char * mot = malloc(sizeof(char) * (STRINGLEN_MAX + 1));
  if (mot == NULL) {
    perror("Problème lors de la création de mot.");
    exit (EXIT_FAILURE);
  }
  
  do {
    if ((c != EOF) && (c != ' ') && (fscanf(f, "%[^ ,!,\n]", mot))) {
      insertInTrie(t, (unsigned char *)mot);
    }
  }
  while ((c = fgetc(f)) != EOF);

  if (fclose(f) != 0) {
    r = FUN_FAILURE;
  }

  return r;
}

//--- Création de la boucle initiale 
void insertion_boucle_etat_zero(Trie t) {
  for (int i = 0; i < UCHAR_MAX; ++i) {
    if (t -> transition[0][i] == EXIST_NO_TRANSITION) {
      t -> transition[0][i] = 0;
    }
  }
}

//--- Compléter le trie 
void completer(Trie t) {
    // on crée la file
    struct _fifo *f = NULL;
    
    // on crée la liste de transiitons
    struct _list *l = NULL;
    
    // on remplit la liste de transitions
    for (int i = 0; i < UCHAR_MAX; ++i) {
        if (t -> transition[0][i] != 0 && t -> transition[0][i] != -1) {
            l = list_insert(l, 0, t -> transition[0][i], i);
        }
    }

    int r = 0;
    int a = 0;
    int p = 0;
    int s = 0;
    struct _list *first_elem_l;
    while (l != NULL) {
        first_elem_l = list_get_first_elem(l);
        r = first_elem_l -> startNode;
        a = first_elem_l -> letter;
        p = first_elem_l -> targetNode;
        l = list_remove(l);
        f = fifo_insert(f, p);
        t -> suppleant[p] = 0;
    }
    
    while (f != NULL) {
        r = fifo_get_first_elem(f);
        f = fifo_remove(f);
        
        for (int i = 0; i < UCHAR_MAX; ++i) {
            if (t -> transition[r][i] != EXIST_NO_TRANSITION) {
                l = list_insert(l, r, t -> transition[r][i], i);
            }
        }
        
        while (l != NULL) {
            first_elem_l = list_get_first_elem(l);
            r = first_elem_l -> startNode;
            a = first_elem_l -> letter;
            p = first_elem_l -> targetNode;
            
            l = list_remove(l);
            f = fifo_insert(f, p);
            s = t -> suppleant[r];
            
            while (t -> transition[s][a] == EXIST_NO_TRANSITION) {
                s = t -> suppleant[s];
            }
            t -> suppleant[p] = t -> transition[s][a];
            
            if (t -> finite[t -> transition[s][a]] != NOT_FINAL) {
                t -> finite[p] = t -> finite[p] + 1;
            }
        }
    }
}


//--- Module d'aide

#define WELCOME "Bienvenue dans l'aide d'Aho-Corsick (matrice). "       \
"Ce programme a pour objectif de trouver les occurences d'un"           \
"mot dans un texte. Nous utilisons ici une matrice. "                   \
"Ce contenu aura les caractéristiques suivantes :"                      \

#define RULE1 "- Le texte comportant les mots sera donné."              \

#define RULE2 "- Le texte dans lequel nous cherchons les mots sera "    \
"donnée."                                                               \

#define CALL "Le programme s'appelle donc avec "                        \
"les deux paramètres suivants : "                                       \

#define ELEM1 "- premier élément : texte comportant les mots"           \

#define ELEM2 "- second élément : texte dans lequel rechercher"         \

#define EXEMPLE "Un exemple serait donc : ./ac-matrice mots.txt "       \
"texte.txt"                                                             \

#define TIME "On obtient le temps d'exécution du programme "            \
"en utilisant la commande --time."                                      \

#define VERBOSE "On obtient un affichage verbeux réduit "               \
"en utilisant la commande --verbose et un affichage verbeux très "      \
"complet en utilisant la commande --verbosefull. "                      \
"L'affichage complet affiche les transitions de 0 vers 0"               \

void aide(const char *progname) {
  fputs("\n" "Module d'aide à l'utilisation de : ", stdout);
  fputs(progname, stdout);
  fputs("\n\n", stdout);

  fputs(WELCOME "\n" RULE1 "\n" RULE2, stdout);
  fputs("\n\n", stdout);

  fputs(CALL "\n" ELEM1 "\n" ELEM2, stdout);
  fputs("\n\n", stdout);

  fputs(EXEMPLE"\n\n", stdout);

  fputs(TIME "\n\n", stdout);

  fputs(VERBOSE "\n", stdout);
}
