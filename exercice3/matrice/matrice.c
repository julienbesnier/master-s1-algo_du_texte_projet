#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include "matrice.h"

#define NOT_FINAL 0
#define FINAL 1

#define EXIST_NO_TRANSITION -1

#define IS_NOT_IN_TRIE 0
#define IS_IN_TRIE 1

#define NO_SUPPLEANT -1

Trie createTrie(int maxNode) {
  if (maxNode <= 0) {
    perror("maxNode est négatif ou nul\n");
    exit(EXIT_FAILURE);
  }

  int **transition = malloc(sizeof(int *) * maxNode);
  if (transition == NULL) {
    perror("Problème malloc transition.");
    exit (EXIT_FAILURE);
  }

  for (int i = 0; i < maxNode; ++i) {
    int *line = malloc(sizeof(int) * UCHAR_MAX);
    if (line == NULL) {
      perror("Problème malloc line.");
      exit (EXIT_FAILURE);
    }

    for (int j = 0; j < UCHAR_MAX; ++j) {
      line[j] = EXIST_NO_TRANSITION; // il n'y a pas de transition
    }

    transition[i] = line;
  }

  int *finite = malloc(sizeof(int) * maxNode);
  if (finite == NULL) {
    perror("Problème malloc finite.");
    exit (EXIT_FAILURE);
  }
  for (int i = 0; i < maxNode; ++i) {
    finite[i] = NOT_FINAL;
  }

  int *suppleant = malloc(sizeof(int) * maxNode);
  if (suppleant == NULL) {
    perror("Problème malloc suppleant.");
    exit (EXIT_FAILURE);
  }
  for (int i = 0; i < maxNode; ++i) {
    suppleant[i] = NO_SUPPLEANT;
  }

  Trie graphe = malloc(sizeof(maxNode) + sizeof(maxNode + 1) 
      + sizeof(transition) + sizeof(finite)
      + sizeof(suppleant));
  if (graphe == NULL) {
    exit (EXIT_FAILURE);
  }

  graphe -> maxNode = maxNode;
  graphe -> nextNode = 1;
  graphe -> transition = transition;
  graphe -> finite = finite;
  graphe -> suppleant = suppleant;

  return graphe;  
}

void insertInTrie(Trie trie, unsigned char *w) {
  if (trie == NULL) {
    perror("Le Trie n'a pas été initialisé.");
    exit (EXIT_FAILURE);
  }

  // printf("Mot à insérer : '%s'\n", w);

  int current_node = 0; // numéro du noeud courant. 
  
  // itération sur le mot reçu
  for (unsigned long i = 0; i < strlen((const char *) w); ++i) {    
    int lettre_lue = w[i];
  	// printf("Lettre lue %c, son rang en ascii %d\n", lettre_lue, lettre_lue);

    // La transition (current_node,lettre_lue) existe-t-elle ?
    if (trie -> transition[current_node][lettre_lue] == EXIST_NO_TRANSITION) { // elle n'existe pas
    	
      // pouvons nous créer un nouveau noeud ?
    	if (trie -> nextNode >= trie -> maxNode) {
    		perror("Il y a déjà le nombre maximal de noeud. Opération incomplète.\n");
    		exit(EXIT_FAILURE);
    	}
    	
      trie -> transition[current_node][lettre_lue] = trie -> nextNode;
      current_node = trie -> nextNode;
      trie -> nextNode = trie -> nextNode + 1;

    } else { // la transition existe
      current_node = trie -> transition[current_node][lettre_lue];
    }
  }

  trie -> finite[current_node] = FINAL;
}

int isInTrie(Trie trie, unsigned char *w) {
  if (trie == NULL) {
    perror("Le Trie n'a pas été initialisé.");
    exit (EXIT_FAILURE);
  }

  printf("Mot à rechercher : '%s'\n", w);

  int current_node = 0; // numéro du noeud courant. 
  
  // itération sur le mot à rechercher
  for (unsigned long i = 0; i < strlen((const char *) w); ++i) {    
    int lettre_lue = w[i];
    
    // La transition (current_node,lettre_lue) existe-t-elle ?
    if (trie -> transition[current_node][lettre_lue] == EXIST_NO_TRANSITION) { // elle n'existe pas
      return IS_NOT_IN_TRIE;
    } else { // la transition existe
      current_node = trie -> transition[current_node][lettre_lue];
    }
  }

  if (trie -> finite[current_node] == NOT_FINAL) {
    return IS_NOT_IN_TRIE;
  } else {
    return IS_IN_TRIE;
  }
}

void matrice_dispose(Trie trie) {
  if (trie != NULL) {
    for (int i = 0; i < trie -> maxNode; ++i) {
      free(trie -> transition[i]);
    }
    free(trie -> finite);
  }
}


// OUTILS

void printTabs(Trie trie) {
	printTransition(trie);
  printf("\n");
	printFinite(trie);
  printf("\n");
  printSuppleant(trie);
}

void printTabsReduced(Trie trie) {
  printTransitionReduced(trie);
  printf("\n");
  printFinite(trie);
  printf("\n");
  printSuppleant(trie);
}

void printTransition(Trie trie) {
	printf("Quelles sont les transitions ? Représentation "
      "de la matrice de transitions :\n");
  printf("Transition |");
  for (int i = 0; i < UCHAR_MAX + 1; ++i) {
    printf(" %c |", i);
  }
  printf("\n");

  for (int i = 0; i < trie -> nextNode; ++i) {
    printf("Etat %2d    |", i);
    for (int j = 0; j < UCHAR_MAX + 1; ++j) {
      if (trie -> transition[i][j] == EXIST_NO_TRANSITION) {
        printf("   |");
      } else {
        printf("%3d|", trie -> transition[i][j]);
      }
    }
    printf("\n");
  }
}

void printTransitionReduced(Trie trie) {
  printf("Quelles sont les transitions ? Représentation "
      "de la matrice de transitions (réduite) :\n");
  printf("LETTRE  |");
  for (int i = 97; i < 122 + 1; ++i) {
    printf(" %c |", i);
  }
  printf("\n");

  for (int i = 0; i < trie -> nextNode; ++i) {
    printf("Etat %2d |", i);
    for (int j = 97; j < 122 + 1; ++j) {
      if (trie -> transition[i][j] == EXIST_NO_TRANSITION) {
        printf("   |");
      } else {
        printf("%3d|", trie -> transition[i][j]);
      }
    }

    printf("\n");
  }
}

void printFinite(Trie trie) {
  printf("Quels noeuds sont finaux ?\n");
  
  printf("ETAT      |");
  for (int i = 0; i < trie -> nextNode; ++i) {
    printf("%3d|", i);
  }
  printf("\n");

  printf("FINAL ?   |");
  for (int i = 0; i < trie -> nextNode; ++i) {
    if (trie -> finite[i] == NOT_FINAL) {
      printf("   |");
    } else {
      printf("%3d|", trie -> finite[i]);
    }
  }
  printf("\n\n");
}

void printSuppleant(Trie trie) {
	printf("Quels sont les suppléants ?\n");
  
  printf("ETAT      |");
  for (int i = 0; i < trie -> nextNode; ++i) {
    printf("%3d|", i);
  }
  printf("\n");

  printf("SUPPLEANT |");
  printf("   |");
  for (int i = 1; i < trie -> nextNode; ++i) {
    if (trie -> suppleant[i] == NO_SUPPLEANT) {
      printf(" - |");
    } else {
      printf("%3d|", trie -> suppleant[i]);
    }
  }
  printf("\n");
}
