#include <stdio.h>
#include <stdlib.h>

#include "../list/list.c"

struct _trie {
   int maxNode;      /* Nombre maximal de noeuds du trie    */
   int nextNode;     /* Indice du prochain noeud disponible */
   List *transition; /* listes d’adjacence                  */
   int *finite;     /* etats terminaux                     */
   int *suppleant;  /* etats suppleants                    */
};

typedef struct _trie *Trie;

// créer un trie (cette primitive prend en entrée le nombre maximal
// de nœuds du trie) ;
Trie createTrie(int maxNode);

// insérer un mot dans un trie
void insertInTrie(Trie trie, unsigned char *w);

// teste si un mot est contenu dans un trie.
// retourne 1 si oui, 0 sinon.
int isInTrie(Trie trie, unsigned char *w);

// nettoye la structure Trie après utilisation
void hash_dispose(Trie trie);

// OUTILS

// retourne la taille d'une table de hachage 
// pour un maxNode donné.
int hashlen(int maxNode);

// fonction de hachage
int hashfun(unsigned char letter, int startNode, int maxNode);

// recherche dans la table de hachage transition 
// une transition de startNode par la lettre l. 
// S'il n'y en a pas, renvoie -1. L'indice de targetNode sinon. 
int search_in_hash(List * transition, unsigned char letter, int startNode, int maxNode);

// Affiche les deux tables de transition et de finitude.
void printTabs(Trie trie);
// Affiche les trois tables de transition (réduite), de finitude et de suppléance.
void printTabsReduced(Trie trie);
// Affiche la table de transition dans le terminal.
void printTransition(Trie trie);
// Affiche la table de transition réduite dans le terminal, 
// c'est à dire sans la boucle de l'état initial (flèches de 0 vers 0).
void printTransitionReduced(Trie trie);
// Affiche la table de finitude dans le terminal.
void printFinite(Trie trie);
// Affiche la table de suppleant dans le terminal.
void printSuppleant(Trie trie);
