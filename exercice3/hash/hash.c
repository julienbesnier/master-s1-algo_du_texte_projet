#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>

#include "hash.h"

#define NOT_FINAL 0
#define FINAL 1

#define EXIST_NO_TRANSITION -1

#define IS_NOT_IN_TRIE 0
#define IS_IN_TRIE 1

#define NO_SUPPLEANT -1

Trie createTrie(int maxNode) {
  if (maxNode <= 0) {
    perror("maxNode est négatif ou nul\n");
    exit(EXIT_FAILURE);
  }

  List *transition = malloc(sizeof(List *) 
    * (unsigned int) hashlen(maxNode));
  if (transition == NULL) {
    perror("Problème malloc transition.");
    exit (EXIT_FAILURE);
  }

  for (int i = 0; i < hashlen(maxNode); ++i) {
    transition[i] = NULL; // il n'y a pas de transition
  }

  int *finite = malloc(sizeof(int) * maxNode);
  if (finite == NULL) {
    perror("Problème malloc finite.");
    exit (EXIT_FAILURE);
  }
  for (int i = 0; i < maxNode; ++i) {
    finite[i] = NOT_FINAL;
  }

  int *suppleant = malloc(sizeof(int) * maxNode);
  if (suppleant == NULL) {
    perror("Problème malloc suppleant.");
    exit (EXIT_FAILURE);
  }
  for (int i = 0; i < maxNode; ++i) {
    suppleant[i] = NO_SUPPLEANT;
  }

  Trie graphe = malloc(sizeof(maxNode) + sizeof(maxNode + 1) 
      + sizeof(transition) + sizeof(finite)
      + sizeof(suppleant));
  if (graphe == NULL) {
    exit (EXIT_FAILURE);
  }

  graphe -> maxNode = maxNode;
  graphe -> nextNode = 1;
  graphe -> transition = transition;
  graphe -> finite = finite;
  graphe -> suppleant = suppleant;

  return graphe;  
}

void insertInTrie(Trie trie, unsigned char *w) {
  if (trie == NULL) {
     perror("Le Trie n'a pas été initialisé.");
     exit (EXIT_FAILURE);
  }

  int current_node = 0; // numéro du noeud courant. 
  
  // itération sur le mot reçu
  for (unsigned long i = 0; i < strlen((const char *) w); ++i) {    
    int lettre_lue = w[i];
  	// printf("Lettre lue %c, son rang en ascii %d\n", lettre_lue, lettre_lue);

    // La transition (current_node,lettre_lue) existe-t-elle ?
    if (search_in_hash(trie -> transition, lettre_lue, current_node, trie -> maxNode) == EXIST_NO_TRANSITION) { 
    	// elle n'existe pas
    	
      	// pouvons nous créer un nouveau noeud ?
    	if (trie -> nextNode >= trie -> maxNode) {
    		 perror("Il y a déjà le nombre maximal de noeud. Opération incomplète.\n");
    		 exit(EXIT_FAILURE);
    	}
      	 int indice = hashfun((unsigned char) lettre_lue, current_node, trie -> maxNode);
         trie -> transition[indice] =
             list_insert(trie -> transition[indice], current_node, trie -> nextNode, w[i]);
      	 current_node = trie -> nextNode;
      	 trie -> nextNode = trie -> nextNode + 1;

    } else { // la transition existe
      current_node = search_in_hash(trie -> transition, (unsigned char) lettre_lue, current_node, trie -> maxNode);
    }
  }
  trie -> finite[current_node] = FINAL;
}

int isInTrie(Trie trie, unsigned char *w) {
  if (trie == NULL) {
    perror("Le Trie n'a pas été initialisé.");
    exit (EXIT_FAILURE);
  }

  int current_node = 0; // numéro du noeud courant. 
  
  // itération sur le mot à rechercher
  for (unsigned long i = 0; i < strlen((const char *) w); ++i) {    
    int lettre_lue = w[i];
    
    // La transition (current_node,lettre_lue) existe-t-elle ?
    int search = search_in_hash(trie -> transition, (unsigned char) lettre_lue, current_node, trie -> maxNode);
    if (search == EXIST_NO_TRANSITION) { // elle n'existe pas
      return IS_NOT_IN_TRIE;
    } else { // la transition existe
      current_node = search;
    }
  }

  if (trie -> finite[current_node] == NOT_FINAL) {
    return IS_NOT_IN_TRIE;
  }
  
  return IS_IN_TRIE;
}


void hash_dispose(Trie trie) {
  if (trie != NULL) {
    for (int i = 0; i < hashlen(trie -> maxNode); ++i) {
      List p = trie -> transition[i];
      while (p != NULL) {
        List t = p;
        p = p -> next;
        free(t);
      }
    }
    free(trie -> finite);
  }
}


// OUTILS

int hashlen(int maxNode) {
  int value = (maxNode * 4) / 3;

  return value;
}

int hashfun(unsigned char letter, int startNode, int maxNode) { 
  // 1777 et 8191 sont deux grands nombres premiers.
  // ils ont été choisis aléatoirement.
  double value = ((startNode + 1777) + letter) * 8191;

  return (int) value % hashlen(maxNode);
}

int search_in_hash(List * transition, unsigned char letter, int startNode, int maxNode) {
  int indice = hashfun(letter, startNode, maxNode);
  struct _list *t = transition[indice];

  return is_in_list(t, letter, startNode);
}

void printTabs(Trie trie) {
	printTransition(trie);
  printf("\n");
	printFinite(trie);
  printf("\n");
  printSuppleant(trie);
}

void printTabsReduced(Trie trie) {
	printTransitionReduced(trie);
  printf("\n");
	printFinite(trie);
  printf("\n");
  printSuppleant(trie);
}

void printTransition(Trie trie) {
  printf("Quelles sont les transitions ? Représentation de la table de hachage\n");
  printf("s : Noeud de départ de la transition\n");
  printf("l : lettre de la transition\n");
  printf("t : Noeud d'arrivée de la transition\n");
  printf("X : fin de la liste\n");
  printf("\n");
  printf("CASES   | -> LISTES DES CELLULES\n");
  for (int i = 0; i < hashlen(trie -> maxNode); ++i) {
    printf("Case %2d | -> ", i);
    List t = trie -> transition[i];
    while (t != NULL) {
      printf("[ s:%2d / l:%c / t:%2d ] -> ", 
        t -> startNode, t -> letter, t -> targetNode);
      t = t -> next;
    }
    printf("X\n");
  }
}

void printTransitionReduced(Trie trie) {
  printf("Quelles sont les transitions ? Représentation de la table de hachage\n");
  printf("s : Noeud de départ de la transition\n");
  printf("l : lettre de la transition\n");
  printf("t : Noeud d'arrivée de la transition\n");
  printf("X : fin de la liste\n");
  printf("\n");
  printf("CASES   | -> LISTES DES CELLULES\n");
  for (int i = 0; i < hashlen(trie -> maxNode); ++i) {
    printf("Case %2d | -> ", i);
    List t = trie -> transition[i];
    while (t != NULL) {
      if (! ((t -> startNode == 0) && (t -> targetNode == 0))) {
        printf("[ s:%2d / l:%c / t:%2d ] -> ", 
        t -> startNode, t -> letter, t -> targetNode);
      }
      t = t -> next;
    }
    printf("X\n");
  }
}

void printFinite(Trie trie) {
  printf("Quels noeuds sont finaux ?\n");
  
  printf("ETAT      |");
  for (int i = 0; i < trie -> nextNode; ++i) {
    printf("%3d|", i);
  }
  printf("\n");

  printf("FINAL ?   |");
  for (int i = 0; i < trie -> nextNode; ++i) {
    if (trie -> finite[i] == NOT_FINAL) {
      printf("   |");
    } else {
      printf("%3d|", trie -> finite[i]);
    }
  }
  printf("\n\n");
}

void printSuppleant(Trie trie) {
	printf("Quels sont les suppléants ?\n");
  
  printf("ETAT      |");
  for (int i = 0; i < trie -> nextNode; ++i) {
    printf("%3d|", i);
  }
  printf("\n");

  printf("SUPPLEANT |");
  printf("   |");
  for (int i = 1; i < trie -> nextNode; ++i) {
    if (trie -> suppleant[i] == NO_SUPPLEANT) {
      printf(" - |");
    } else {
      printf("%3d|", trie -> suppleant[i]);
    }
  }
  printf("\n");
}
