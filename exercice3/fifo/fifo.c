#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include "fifo.h"

struct _fifo *fifo_insert(struct _fifo *fifo, int value) {
  struct _fifo *p = malloc(sizeof(struct _fifo));
  if (p == NULL) {
    perror("Problème lors de la création de la cellule.");
    exit (EXIT_FAILURE);
  }

  p -> value = value;
  p -> next = fifo;

  return p;
}

int fifo_get_first_elem(struct _fifo *fifo) {
  if (fifo == NULL) {
    perror("Problème : il n'y a pas d'élément dans la file.");
    exit(EXIT_FAILURE);
  }

  if (fifo -> next == NULL) {
    return fifo -> value;
  }

  struct _fifo *current = fifo -> next;

  while (current -> next != NULL) {
    current = current -> next;
  }

  return (current -> value);
}

void *fifo_remove(struct _fifo *fifo) {
  if (fifo == NULL) {
    perror("Problème : il n'y a pas d'élément dans la file.");
    exit(EXIT_FAILURE);
  }

  if (fifo -> next == NULL) {
    return NULL;
  }

  struct _fifo *last = fifo;
  struct _fifo *current = fifo -> next;

  while (current -> next != NULL) {
    last = current;
    current = current -> next;
  }

  last -> next = NULL;
  free(current);

  return fifo;
}

void fifo_print(struct _fifo *fifo) {
  if (fifo == NULL) {
    printf("Fifo -> X\n");
    return;
  }

  printf("Fifo -> ");
  struct _fifo *current = fifo;
  while (current != NULL) {
    printf("[ %d ] -> ", current -> value);
    current = current -> next;
  }

  printf("X\n");
}
