#include <stdio.h>
#include <stdlib.h>

struct _fifo {
   int value;            /* valeur                          */
   unsigned char letter; /* etiquette de la transition      */
   struct _fifo *next;   /* maillon suivant                 */
};

typedef struct _fifo *Fifo;

//  fifo_insert : insère value en tête de la file associée à fifo. 
//    Renvoie la liste nouvellement formée.
struct _fifo *fifo_insert(struct _fifo *fifo, int value);

// fifo_remove : retourne la valeur en tête de la file fifo.
int fifo_get_first_elem(struct _fifo *fifo);

//  fifo_remove : dépile l'élément en queue de la file associée à fifo. 
void *fifo_remove(struct _fifo *fifo);

//  fifo_print : affiche la file associée à fifo.
void fifo_print(struct _fifo *fifo);
