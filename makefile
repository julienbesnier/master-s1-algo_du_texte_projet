CFLAGS = -std=c11 -Werror -Wextra -Wall -pedantic
LDFLAGS =
LIBS =
objects1 = exercice1/main.o
objects2 = exercice2/main.o
objects3 = exercice3/matrice/main.o
objects4 = exercice3/hash/main.o

executable1 = genere-texte
executable2 = genere-mots
executable3 = ac-matrice
executable4 = ac-hachage

all: executable1 executable2 executable3 executable4


executable1: $(objects1)
	$(CC) $(CFLAGS) $(LDFLAGS) $(objects1) -o $(executable1) $(LIBS)
executable2: $(objects2)
	$(CC) $(CFLAGS) $(LDFLAGS) $(objects2) -o $(executable2) $(LIBS)
executable3: $(objects3)
	$(CC) $(CFLAGS) $(LDFLAGS) $(objects3) -o $(executable3) $(LIBS)
executable4: $(objects4)
	$(CC) $(CFLAGS) $(LDFLAGS) $(objects4) -o $(executable4) $(LIBS)


objects1: exercice1/main.c exercice1/generator.c exercice1/generator.h
		$(CC) -c $(CFLAGS) exercice1/main.c

objects2: exercice2/main.c exercice2/generator.c exercice2/generator.h
		$(CC) -c $(CFLAGS) exercice2/main.c

objects3: exercice3/matrice/main.c exercice3/matrice/matrice.c exercice3/matrice/matrice.h exercice3/fifo.c exercice3/fifo.h
		$(CC) -c $(CFLAGS) exercice3/matrice/main.c

objects4: exercice3/hash/main.c exercice3/hash/hash.c exercice3/hash/hash.h exercice3/fifo.c exercice3/fifo.h
		$(CC) -c $(CFLAGS) exercice3/hash/main.c

clean:
		$(RM) exercice1/*.o exercice1/*.gch $(executable1)
		$(RM) exercice2/*.o exercice2/*.gch $(executable2)
		$(RM) exercice3/*.o exercice3/*.gch $(executable3)
		$(RM) exercice3/matrice/*.o exercice3/matrice/*.gch $(executable3)
		$(RM) exercice3/hash/*.o exercice3/hash/*.gch $(executable4)
