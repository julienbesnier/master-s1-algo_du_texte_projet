#ifndef GENERATOR_H
#define GENERATOR_H

void complex_generator(const size_t words_number, const size_t word_minimal_size, 
    const size_t word_maximal_size, const size_t alphabet_length);
char generate_letter(size_t length);

#endif //GENERATOR_H
