#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>

#include "generator.c"

void aide(const char *progname);

//--- fonction principale

#define ARG__OPT_LONG "--"
#define ARG_HELP      "help"
#define ARG_TIME      "time"

int main(int argc, char *argv[]) {
  int do_show_time = 0;
  
  for (int k = 1; k < argc; ++k) {
    const char *a = argv[k];
    if (strlen(a) > strlen(ARG__OPT_LONG)
        && strlen(a) <= strlen(ARG__OPT_LONG ARG_HELP)
        && strncmp(a, ARG__OPT_LONG ARG_HELP, strlen(a)) == 0) {
      aide(argv[0]);
      return EXIT_SUCCESS;
    }

    if (strlen(a) > strlen(ARG__OPT_LONG)
        && strlen(a) <= strlen(ARG__OPT_LONG ARG_TIME)
        && strncmp(a, ARG__OPT_LONG ARG_TIME, strlen(a)) == 0) {
      do_show_time = 1;
    }
  }
  
  if (argc != 5) {
    if (argc != 6 && do_show_time == 0) {
      printf("Erreur : veuillez entrer quatre valeurs.\n");
      printf("Pour plus d'informations, veuillez utiliser la commande --help\n");

      exit(EXIT_FAILURE);
    }
  }
  
  complex_generator(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));

  if (do_show_time == 1) {
    printf("\nLe programme a mis %f secondes à s'exécuter.\n\n",
      (double) clock()/CLOCKS_PER_SEC);
  }

  return EXIT_SUCCESS;
}

//--- Module d'aide
#define WELCOME "Bienvenue dans l'aide du générateur de mots. "         \
"Ce programme a pour objectif de générer une suite de mots. "           \
"Ce contenu aura les caractéristiques suivantes :"                      \

#define RULE1 "- Le nombre de mots sera donné."                         \

#define RULE2 "- La longueur minimale des mots sera donnée."            \

#define RULE3 "- La longueur maximale des mots sera donnée."            \

#define RULE4 "- L'alphabet aura une longueur donnée."                  \

#define CALL "Le programme s'appelle donc avec "                        \
"les quatres paramètres suivants : "                                    \

#define ELEM1 "- premier élément : nombre de mots à générer"            \

#define ELEM2 "- second élément : taille minimale des mots"             \

#define ELEM3 "- troisième élément : taille maximale des mots"          \

#define ELEM4 "- quatrième élément : taille de l'alphabet"              \

#define EXEMPLE1 "Un exemple serait donc : ./genere-mots 150 8 10 5"    \

#define EXEMPLE2 "On obtiendrait une suite de 150 mots "                \
"de longueur 8 à 10 (en nombre de caractères) "                         \
"sur un alphabet de longueur 5"                                         \

#define TIME "On obtient le temps d'exécution du programme "            \
"en utilisant la commande --time."                                      \

void aide(const char *progname) {
  fputs("\n" "Module d'aide à l'utilisation de : ", stdout);
  fputs(progname, stdout);
  fputs("\n\n", stdout);

  fputs(WELCOME "\n" RULE1 "\n" RULE2 "\n" RULE3 "\n" RULE4, stdout);
  fputs("\n\n", stdout);

  fputs(CALL "\n" ELEM1 "\n" ELEM2 "\n" ELEM3 "\n" ELEM4, stdout);
  fputs("\n\n", stdout);

  fputs(EXEMPLE1 "\n" EXEMPLE2 "\n\n", stdout);

  fputs(TIME "\n", stdout);
}
