#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <time.h>

#include "generator.h"

void complex_generator(const size_t words_number, const size_t word_minimal_size, 
    const size_t word_maximal_size, const size_t alphabet_length) {
  
  if (word_minimal_size > word_maximal_size) {
    printf("Erreur : La longueur minimale doit être supérieure ou égale à la longueur maximale\n");

    exit(EXIT_FAILURE);
  }

  int diff = word_maximal_size - word_minimal_size;
  int alea;
  srand(time(NULL));

  for (size_t i = 0; i < words_number; ++i) {
    alea = rand() % (diff + 1) + word_minimal_size;
    for (int j = 0; j < alea; ++j) {
      printf("%c", generate_letter(alphabet_length));
    }
    if (i < words_number - 1) {
      printf("%s", " ");
    }
  }
}

// OUTILS
char generate_letter(size_t length) {
  int number = (int) rand() % length;
  
  return (char) 48 + number;
}
