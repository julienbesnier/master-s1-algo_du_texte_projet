Module d'aide du projet d'algorithmique du texte de Julien BESNIER :


Module d'aide à l'utilisation de : ./genere-texte

Bienvenue dans l'aide du générateur de texte. Ce programme a pour objectif de générer un texte. Le texte aura les caractéristiques suivantes :
- Le texte sera d'une longueur donnée.
- L'alphabet aura une longueur donnée.

Le programme s'appelle donc avec les deux paramètres suivants : 
- premier élément : longueur du texte à générer
- second élément : taille de l'alphabet

Un exemple serait donc : ./genere-texte 150 10
On obtiendrait ainsi un texte de longueur 150 sur un alphabet de longueur 10.

On obtient le temps d'exécution du programme en utilisant la commande --time.

On obtient ce module d'aide en utilisant la commande --help.



Module d'aide à l'utilisation de : ./genere-mots

Bienvenue dans l'aide du générateur de mots. Ce programme a pour objectif de générer une suite de mots. Ce contenu aura les caractéristiques suivantes :
- Le nombre de mots sera donné.
- La longueur minimale des mots sera donnée.
- La longueur maximale des mots sera donnée.
- L'alphabet aura une longueur donnée.

Le programme s'appelle donc avec les quatres paramètres suivants : 
- premier élément : nombre de mots à générer
- second élément : taille minimale des mots
- troisième élément : taille maximale des mots
- quatrième élément : taille de l'alphabet

Un exemple serait donc : ./genere-mots 150 8 10 5
On obtiendrait une suite de 150 mots de longueur 8 à 10 (en nombre de caractères) sur un alphabet de longueur 5

On obtient le temps d'exécution du programme en utilisant la commande --time.

On obtient ce module d'aide en utilisant la commande --help.



Module d'aide à l'utilisation de : ./ac-matrice

Bienvenue dans l'aide d'Aho-Corsick (matrice). Ce programme a pour objectif de trouver les occurences d'unmot dans un texte. Nous utilisons ici une matrice. Ce contenu aura les caractéristiques suivantes :
- Le texte comportant les mots sera donné.
- Le texte dans lequel nous cherchons les mots sera donnée.

Le programme s'appelle donc avec les deux paramètres suivants : 
- premier élément : texte comportant les mots
- second élément : texte dans lequel rechercher

Un exemple serait donc : ./ac-matrice mots.txt texte.txt

On obtient le temps d'exécution du programme en utilisant la commande --time.

On obtient un affichage verbeux réduit en utilisant la commande --verbose et un affichage verbeux très complet en utilisant la commande --verbosefull. L'affichage complet affiche les transitions de 0 vers 0.

On obtient ce module d'aide en utilisant la commande --help.



Module d'aide à l'utilisation de : ./ac-hachage

Bienvenue dans l'aide d'Aho-Corsick (hachagee). Ce programme a pour objectif de trouver les occurences d'unmot dans un texte. Nous utilisons ici une table de hachage. Ce contenu aura les caractéristiques suivantes :
- Le texte comportant les mots sera donné.
- Le texte dans lequel nous cherchons les mots sera donnée.

Le programme s'appelle donc avec les deux paramètres suivants : 
- premier élément : texte comportant les mots
- second élément : texte dans lequel rechercher

Un exemple serait donc : ./ac-hachage mots.txt texte.txt

On obtient le temps d'exécution du programme en utilisant la commande --time.

On obtient un affichage verbeux réduit en utilisant la commande --verbose et un affichage verbeux très complet en utilisant la commande --verbosefull. L'affichage complet affiche les transitions de 0 vers 0.

On obtient ce module d'aide en utilisant la commande --help.